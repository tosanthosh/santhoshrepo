package com.ubs.opsit.interviews;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

public class TimeConverterUnitTest {
	@Test
	public void testConvertTime_Midnight() {
		TimeConverter timeConverter = new TimeConverterImpl();
		String result = timeConverter.convertTime(BerlinClockEnum.MIDNIGHT.time());
		assertNotNull(result);
		assertEquals(BerlinClockEnum.MIDNIGHT.result(), result);
	}

	@Test
	public void testConvertTime_MidAfternoon()  {
		TimeConverter timeConverter = new TimeConverterImpl();
		String result = timeConverter.convertTime(BerlinClockEnum.MIDAFTERNOON.time());
		assertNotNull(result);
		assertEquals(BerlinClockEnum.MIDAFTERNOON.result(), result);
	}

	@Test
	public void testConvertTime_BeforeMidnight() {
		TimeConverter timeConverter = new TimeConverterImpl();
		String result = timeConverter.convertTime(BerlinClockEnum.BEFOREMIDNIGHT.time());
		assertNotNull(result);
		assertEquals(BerlinClockEnum.BEFOREMIDNIGHT.result(), result);
	}

	@Test
	public void testConvertTime_LateNight() {
		TimeConverter timeConverter = new TimeConverterImpl();
		String result = timeConverter.convertTime(BerlinClockEnum.LATEMIDNIGHT.time());
		assertNotNull(result);
		assertEquals(BerlinClockEnum.LATEMIDNIGHT.result(), result);
	}

	@Test
	public void testConvertTime_NullCheck() {
		TimeConverter timeConverter = new TimeConverterImpl();
		String result = timeConverter.convertTime(null);
		assertNull(result);
	}
}
