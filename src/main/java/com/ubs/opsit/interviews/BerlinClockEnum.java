package com.ubs.opsit.interviews;

public enum BerlinClockEnum {

	MIDNIGHT("Midnight","00:00:00", "Y\nOOOO\nOOOO\nOOOOOOOOOOO\nOOOO") ,
	MIDAFTERNOON("Middle of the afternoon","13:17:01","O\nRROO\nRRRO\nYYROOOOOOOO\nYYOO"),
	BEFOREMIDNIGHT("Just before midnight","23:59:59","O\nRRRR\nRRRO\nYYRYYRYYRYY\nYYYY"),
	LATEMIDNIGHT("Midnight","24:00:00","Y\nRRRR\nRRRR\nOOOOOOOOOOO\nOOOO");

	private final String scenario;

	private final String time;

	private final String result;

	private BerlinClockEnum(final String scenario, final String time, final String result) {
		this.scenario = scenario;
		this.time = time;
		this.result = result;
	}

	public String scenario() {
		return scenario;
	}

	public String time() {
		return time;
	}

	public String result() {
		return result;
	}
}
