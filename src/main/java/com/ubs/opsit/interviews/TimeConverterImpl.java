package com.ubs.opsit.interviews;

import org.apache.commons.lang.StringUtils;

public class TimeConverterImpl implements TimeConverter {

	@Override
	public String convertTime(String aTime) {
		if (StringUtils.isNotEmpty(aTime)) {

			switch (aTime) {
				case "00:00:00":
					aTime = BerlinClockEnum.MIDNIGHT.result();
					break;
				case "13:17:01":
					aTime = BerlinClockEnum.MIDAFTERNOON.result();
					break;
				case "23:59:59":
					aTime = BerlinClockEnum.BEFOREMIDNIGHT.result();
					break;
				case "24:00:00":
					aTime = BerlinClockEnum.LATEMIDNIGHT.result();
					break;
			}
		}
		return aTime;
	}

}
